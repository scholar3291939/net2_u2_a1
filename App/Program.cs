﻿// See https://aka.ms/new-console-template for more information

using App;

namespace Champions {

    class Program
    {
        static void Main(string[] args)
        {
            // Inicializa los datos del programa
            InicializaDatos();
            // Muestra el menú principal
            Menu();
        }

        public static void Menu()
        {

            while (true)
            {
                Console.WriteLine(
                    "Selecciona una de las opciones para mostrar acerca de la competencia.\r\n" +
                    "1. Mostrar Equipos\r\n" +
                    "2. Mostrar Masajistas\r\n" +
                    "3. Mostrar Entrenadores\r\n" +
                    "4. Mostrar Plan de Equipo\r\n" +
                    "5. Mostrar Convocatoria de Entrenamiento\r\n" +
                    "6. Salir");

                int opcion = int.Parse(Console.ReadLine());

                switch (opcion)
                {
                    case 1:

                        MostrarEquipos();
                        break;

                    case 2:
                        MostrarMasajistas();
                        break;

                    case 3:
                        MostrarEntrenadores();
                        break;

                    case 4:
                        MostrarPlanEquipo();
                        break;

                    case 5:
                        MostrarConvocatoriaEntrenamiento();
                        break;
                }

                if (opcion.Equals(6))
                {
                    Console.Clear();
                    Console.WriteLine("VUELVA PRONTO");
                    Thread.Sleep(3000);
                }
            }
        }

        // Función para inicializar los datos del programa
        public static void InicializaDatos()
        {
            // Inicializa los datos de los clubes, futbolistas, masajistas y entrenadores
            clubes = new ClubFutbol[2];
            ClubFutbol realMadrid = new ClubFutbol(1, "Real Madrid", System.DateOnly.Parse("01-01-1900"), 15);
            ClubFutbol barcelona = new ClubFutbol(2, "FC Barcelona", DateOnly.Parse("01-01-1934"), 26);
            clubes[0] = realMadrid;
            clubes[1] = barcelona;


            futbolistas = new Futbolista[10];
            futbolistas[0] = new Futbolista(realMadrid.Id, realMadrid.ClubName, realMadrid.CreationDate, realMadrid.NumberOfTitles,
                "Karim Benzema", 9, "Hafid", "Benzema", DateOnly.Parse("19-12-1987"), "Delantero");
            futbolistas[1] = new Futbolista(realMadrid.Id, realMadrid.ClubName, realMadrid.CreationDate, realMadrid.NumberOfTitles,
                "Luka Modric", 10, "Ivica", "Modrić", DateOnly.Parse("09-09-1985"), "Centrocampista");
            futbolistas[2] = new Futbolista(realMadrid.Id, realMadrid.ClubName, realMadrid.CreationDate, realMadrid.NumberOfTitles,
                "Thibaut Courtois", 1, "Thierry", "Courtois", DateOnly.Parse("11-05-1992"), "Portero");
            futbolistas[3] = new Futbolista(realMadrid.Id, realMadrid.ClubName, realMadrid.CreationDate, realMadrid.NumberOfTitles,
                "Vinicius Junior", 20, "Vinicius", "José Paixão de Oliveira Júnior", DateOnly.Parse("12-06-2000"), "Delantero");
            futbolistas[4] = new Futbolista(realMadrid.Id, realMadrid.ClubName, realMadrid.CreationDate, realMadrid.NumberOfTitles,
                "Casemiro", 18, "Carlos", "Henrique Casemiro", DateOnly.Parse("23-02-1986"), "Centrocampista");
            futbolistas[5] = new Futbolista(barcelona.Id, barcelona.ClubName, barcelona.CreationDate, barcelona.NumberOfTitles,
                "Pedri", 8, "Pedro", "González López", DateOnly.Parse("05-11-2002"), "Centrocampista");
            futbolistas[6] = new Futbolista(barcelona.Id, barcelona.ClubName, barcelona.CreationDate, barcelona.NumberOfTitles,
                "Gerard Piqué", 3, "Gerard", "Piqué Bernabeu", DateOnly.Parse("02-02-1987"), "Defensa");
            futbolistas[7] = new Futbolista(barcelona.Id, barcelona.ClubName, barcelona.CreationDate, barcelona.NumberOfTitles,
                "Ansu Fati", 10, "Anssumane", "Pedri", DateOnly.Parse("31-10-2002"), "Delantero");
            futbolistas[8] = new Futbolista(barcelona.Id, barcelona.ClubName, barcelona.CreationDate, barcelona.NumberOfTitles,
                "Frenkie de Jong", 21, "Frenkie", "de Jong", DateOnly.Parse("12-11-1997"), "Centrocampista");
            futbolistas[9] = new Futbolista(barcelona.Id, barcelona.ClubName, barcelona.CreationDate, barcelona.NumberOfTitles,
                "Marc-André ter Stegen", 1, "Marc-André", "ter Stegen", DateOnly.Parse("30-04-1992"), "Portero");


            masajistas = new Masajista[3];
            masajistas[0] = new Masajista(barcelona.Id, barcelona.ClubName, barcelona.CreationDate, barcelona.NumberOfTitles, "Fisioterapia Deportiva", 10);
            masajistas[1] = new Masajista(barcelona.Id, barcelona.ClubName, barcelona.CreationDate, barcelona.NumberOfTitles, "Readaptación Física", 8);
            masajistas[2] = new Masajista(realMadrid.Id, realMadrid.ClubName, realMadrid.CreationDate, realMadrid.NumberOfTitles, "Masaje Deportivo", 5);


            entrenadores = new Entrenador[2];
            entrenadores[0] = new Entrenador(barcelona.Id, barcelona.ClubName, barcelona.CreationDate, barcelona.NumberOfTitles, 12345);
            entrenadores[1] = new Entrenador(realMadrid.Id, realMadrid.ClubName, realMadrid.CreationDate, realMadrid.NumberOfTitles, 54321);
        }

        // Función para mostrar la información de los equipos
        public static void MostrarEquipos()
        {
            // Muestra la información de los equipos

            Console.Clear();
            Console.WriteLine($"\n\nA continuacion se muestran los equipos que estaran participando en la contienda");
            foreach (ClubFutbol club in clubes) {
                club.Presentar();
            }
            Thread.Sleep(4000);
            Console.Clear();
        }

        // Función para mostrar la información de los entrenadores
        public static void MostrarEntrenadores()
        {
            // Muestra la información de los entrenadores
            Console.Clear();
            Console.WriteLine($"\n\nA continuacion se muestran los entrenamdores que estaran dirigiendo en el entrenamiento/juego");
            foreach (Entrenador entrenador in entrenadores)
            {
                entrenador.Presentar();
            }
            Thread.Sleep(4000);
            Console.Clear();
        }

        // Función para mostrar la información de los masajistas
        public static void MostrarMasajistas()
        {
            // Muestra la información de los masajistas
            Console.Clear();
            Console.WriteLine($"\n\nA continuacion se muestran los masajistas que estaran auxiiliando en el entrenamiento/juego");
            foreach (Masajista masajista in masajistas)
            {
                masajista.Presentar();
            }
            Thread.Sleep(4000);
            Console.Clear();
        }

        // Función para mostrar el plan de equipo
        public static void MostrarPlanEquipo()
        {
            // Muestra el plan de equipo
            Console.Clear();
            Console.WriteLine($"\n\nA continuacion se muestran las alineaciones de los equipos");

            foreach (ClubFutbol club in clubes)
            {

                Console.WriteLine($"\n\nAlineacion el club {club.ClubName}");

                foreach (Futbolista futbolista in futbolistas)
                {
                    if (futbolista.ClubName.Equals(club.ClubName))
                    {
                        futbolista.Presentar();
                    }
                }

                foreach (Masajista masajista in masajistas)
                {
                    if (masajista.ClubName == club.ClubName)
                    {
                        masajista.Presentar();
                    }

                }
            }
            Thread.Sleep(7000);
            Console.Clear();
        }

        // Función para mostrar la convocatoria de entrenamiento
        public static void MostrarConvocatoriaEntrenamiento()
        {
            // Muestra la convocatoria de entrenamiento
            Console.Clear();
            Console.WriteLine($"\n\nA continuacion se muestran los planes de entrenamiento de cada equipo");

            foreach (ClubFutbol club in clubes)
            {

                Console.WriteLine($"\n\nAlineacion el club {club.ClubName}");

                foreach (Futbolista futbolista in futbolistas)
                {
                    if (futbolista.ClubName.Equals(club.ClubName))
                    {
                        if (rand.NextInt64(2).Equals(1))
                        {
                            futbolista.Entrenar();
                        }
                        else {
                            futbolista.Concentrarse();
                        }

                        if (rand.NextInt64(2).Equals(1))
                        {
                            futbolista.Viajar();
                        }
                        else
                        {
                            futbolista.JugarPartido();
                        }

                    }
                }
            }
            Thread.Sleep(7000);
            Console.Clear();
        }

        public static Entrenador[] entrenadores;
        public static Futbolista[] futbolistas;
        public static ClubFutbol[] clubes;
        public static Masajista[] masajistas;
        public static Random rand = new Random();

    }
}