﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace App
{
    internal class ClubFutbol
    {
        protected int _id;  // Campo protegido para el ID del club
        protected string _name;  // Campo protegido para el nombre del club
        protected DateOnly _creationDate;  // Campo protegido para la fecha de creación del club
        protected int _numberOfTitles;  // Campo protegido para el número de títulos del club


        public ClubFutbol(
            int id, string clubName, DateOnly creationDate, int numberOfTitles
        )
        {
            Id = id;
            ClubName = clubName;
            CreationDate = creationDate;
            NumberOfTitles = numberOfTitles;
        }

        public int Id
        {
            get => _id;
            set => _id = value;
        }

        public string ClubName
        {
            get => _name;
            set => _name = value;
        }

        public DateOnly CreationDate
        {
            get => _creationDate;
            set => _creationDate = value;
        }

        public int NumberOfTitles
        {
            get => _numberOfTitles;
            set => _numberOfTitles = value;
        }

        // Método virtual para viajar del club de fútbol
        public virtual void Viajar()
        {
            System.Console.WriteLine($"El jugador debe viajar");
        }

        // Método virtual para concentrarse del club de fútbol
        public virtual void Concentrarse()
        {
            System.Console.WriteLine($"El jugador debe presentarse a la concentracion");
        }

        // Método virtual para presentar información del club de fútbol
        public virtual void Presentar()
        {
            System.Console.WriteLine($"Club {_name} con id {_id} y fecha de creacion {_creationDate} ha acumulado {_numberOfTitles} titulos");
        }
    }
}