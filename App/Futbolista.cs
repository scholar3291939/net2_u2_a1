﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App
{
    internal class Futbolista : ClubFutbol
    {
        public int Id { get; private set; }
        public string Name { get; private set; }
        public DateOnly DateOfBirth { get; private set; }
        public int DorsalNumber { get; private set; }
        public string FatherLastname { get; private set; }
        public string MotherLastname { get; private set; }
        public string Position { get; private set; }

        // Constructor para inicializar las propiedades del futbolista
        public Futbolista(int id, string clubName, DateOnly creationDate, int numberOfTitles,
                          string name, int dorsalNumber, string fatherLastname, string motherLastname, DateOnly dateOfBirth, string position)
            : base(id, clubName, creationDate, numberOfTitles)
        {
            Id = id; 
            Name = name;
            DateOfBirth = dateOfBirth;
            DorsalNumber = dorsalNumber;
            FatherLastname = fatherLastname;
            MotherLastname = motherLastname;
            Position = position;
        }

        // Método para que el futbolista entrene
        public void Entrenar()
        {
            System.Console.WriteLine($"El jugador {Name} {FatherLastname} {MotherLastname} con numero {DorsalNumber} esta convocado a entrenar");
        }

        // Método para que el futbolista juegue un partido
        public void JugarPartido()
        {
            System.Console.WriteLine($"El jugador {Name} {FatherLastname} {MotherLastname} con numero {DorsalNumber} esta convocado a jugar el proximo partido en la posicion de {Position}");
        }

        // Método para presentar al futbolista y su equipo
        public override void Presentar()
        {
            System.Console.WriteLine($"El futbolista {Name} esta jugando para el equipo {base.ClubName} en la posicion de {Position}");
        }

        // Método para que el futbolista viaje
        public override void Viajar()
        {
            System.Console.WriteLine($"El jugador {Name} {FatherLastname} {MotherLastname} debe viajar");
        }

        // Método para que el futbolista se concentre
        public void Concentrarse()
        {
            System.Console.WriteLine($"El jugador {Name} {FatherLastname} {MotherLastname} debe presentarse a la concentracion");
        }
    }
}
