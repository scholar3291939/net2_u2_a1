﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace App
{
    internal class Entrenador : ClubFutbol // Clase Entrenador que hereda de ClubFutbol
    {
        // Propiedades de la clase Entrenador
        public int IdFederacion { get; private set; }
        public string Nombre { get; private set; }
        public string Apellidos { get; private set; }
        public DateOnly FechaNacimiento { get; private set; }

        // Constructor de la clase Entrenador
        public Entrenador(int id, string clubName, DateOnly creationDate, int numberOfTitles,
                            int idFederacion)
            : base(id, clubName, creationDate, numberOfTitles)
        {
            IdFederacion = idFederacion;
        }

        // Método para dirigir un entrenamiento
        public void DirigirEntrenamiento()
        {
            System.Console.WriteLine($"El entrenador con ID de federación {IdFederacion} está dirigiendo el entrenamiento");
        }

        // Método para dirigir un partido
        public void DirigirPartido()
        {
            System.Console.WriteLine($"El entrenador con ID de federación {IdFederacion} está dirigiendo el partido");
        }

        // Implementación del método Presentar de la clase base
        public override void Presentar()
        {
            System.Console.WriteLine($"El entrenador con ID de federación {IdFederacion} está dirigiendo al equipo {base.ClubName}");
        }
    }
}