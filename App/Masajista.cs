﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App
{
    internal class Masajista : ClubFutbol
    {
        public string EspecialidadMedica { get; private set; }
        public int AnosExperiencia { get; private set; }

        public Masajista(int id, string clubName, DateOnly creationDate, int numberOfTitles,
                          string especialidadMedica, int anosExperiencia)
            : base(id, clubName, creationDate, numberOfTitles)
        {
            EspecialidadMedica = especialidadMedica;
            AnosExperiencia = anosExperiencia;
        }

        public void AplicarMasaje()
        {
            System.Console.WriteLine($"El masajista con especialidad {EspecialidadMedica} y {AnosExperiencia} años de experiencia está aplicando un masaje");
        }

        public override void Presentar()
        {
            System.Console.WriteLine($"El masajista con especialidad {EspecialidadMedica} y {AnosExperiencia} años de experiencia trabaja para el {base.ClubName} club");
        }
    }
}